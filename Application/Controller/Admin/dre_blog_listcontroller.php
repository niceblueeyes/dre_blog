<?php

namespace Bender\dre_Blog\Application\Controller\Admin;

class dre_blog_listcontroller extends \OxidEsales\Eshop\Application\Controller\Admin\AdminListController
{
    protected $_oBlogList = null;

    public function render()
    {
        $ret = parent::render();
        $this->_oBlogList = oxNew(\Bender\dre_Blog\Application\Model\bloglist::class);
        $this->_oBlogList->loadBlogListe();
        return 'dre_blog_list.tpl';
    }

    public function getBlogList(){
        return $this->_oBlogList;
    }
}