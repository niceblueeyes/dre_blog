<?php

namespace Bender\dre_Blog\Application\Controller;

use OxidEsales\Eshop\Core\Request;
use OxidEsales\Eshop\Core\Registry;

class dre_bloglist extends \OxidEsales\Eshop\Application\Controller\FrontendController
{
    protected $_sThisTemplate = 'dre_bloglist.tpl';

    protected $_oActionList = null;

    protected $_oBlogList = null;

    protected $_oAction = null;

    public function render()
    {
        $this->_oBlogList = oxNew(\Bender\dre_Blog\Application\Model\bloglist::class);
        $this->_oBlogList->loadBlogListe();
        parent::render();
        return $this->_sThisTemplate;
    }

    public function getBlogList()
    {
        return $this->_oBlogList;
    }



}