<?php

namespace Bender\dre_Blog\Application\Controller;

use mysql_xdevapi\Exception;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Request;
use OxidEsales\Eshop\Core\Registry;

class dre_blog extends \OxidEsales\Eshop\Application\Controller\FrontendController
{
    protected $_sThisTemplate = 'dre_blog.tpl';

    protected $_oActionList = null;

    protected $_oBlogList = null;

    protected $blogid = null;

    protected $_oBlog = null;

    protected $_oAction = null;

    public function render()
    {
        /*
        $request = oxNew(Request::class);
        $sAction = $request->getRequestParameter('bloglist');

        if (!empty($sAction)) {
            $this->_oActionList = oxNew('oxarticlelist');
            $this->_oActionList->loadActionArticles($sAction);

            $this->_oAction = oxNew('oxblog');
            $this->_oAction->load($sAction);
        }else{
            //TODO: return blog overview List
            #$this->_oActionList = oxNew('BlogList');
            #$this->_oActionList->loadBlogListe();
        }
*/
        $request = oxNew(Request::class);
        $blogid = $request->getRequestParameter('blogid');
        if($blogid == null){
            $blogid = $this->getLatestBlog();
        }
        $this->_oBlog = oxNew(\Bender\dre_Blog\Application\Model\oxblog::class);
        $this->_oBlog->load($blogid);
        /*
        $this->_oBlogList = oxNew(\Bender\dre_Blog\Application\Model\bloglist::class);
        $this->_oBlogList->loadBlogListe();
        */
        parent::render();
        return $this->_sThisTemplate;
        //return parent::render();
    }

    public function getBlog(){
        return $this->_oBlog;
    }

    public function getLatestBlog(){
        $odB = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $query = "SELECT OXID FROM oxblog ORDER BY OXTIMESTAMP DESC";
        try{
            $oxid = $odB->getOne($query);
        }catch (Exception $e){
            echo $e;
            die();
        }

        return $oxid;
    }

    public function getTitle()
    {
        if ($this->_oBlog != null) {
            return $this->_oBlog->oxblog__oxtitle->value;
        }
    }

    public function getShortDesc(){
        if ($this->_oBlog != null) {
            return $this->_oBlog->oxblog__oxshortdesc->value;
        }
    }

    public function getPicture(){
        if ($this->_oBlog != null) {
            return $this->_oBlog->oxblog__oxpic->value;
        }
    }


    public function getContent()
    {
        if ($this->_oBlog != null) {
            $sContent = $this->_oBlog->getLongDesc();
            return Registry::get("oxUtilsView")->parseThroughSmarty($sContent);
        }
    }

    public function getActionProducts()
    {
        return $this->_oActionList;
    }

    public function getBreadCrumb()
    {
        $aPaths = array();
        $aPath = array();
        $aPath['title'] = $this->getTitle();
        $aPath['link'] = $this->getLink();
        $aPaths[] = $aPath;
        return $aPaths;
    }

    public function getLink($iLang = null)
    {
        if (!isset($iLang)) {
            $iLang = Registry::getLang()->getBaseLanguage();
        }

        $oConf = $this->getConfig();

        $request = oxNew(Request::class);
        $sblogid = $request->getRequestParameter('blogid');

        $requestparams = null;
        $url = 'cl=dre_blog&amp';


        $sUrl = 'index.php?cl=dre_blog&amp;blogid=' . $sblogid;

        $oSeoEncoder = oxNew('oxseoencoder');
        $sSeoUrl = $oSeoEncoder->fetchSeoUrl($sUrl, $iLang);

        if ($sSeoUrl) {
            return $oConf->getCurrentShopUrl() . $sSeoUrl;
        }

        $aParams['blogid'] = $sblogid;
        return Registry::get("oxUtilsUrl")->processUrl($oConf->getShopCurrentURL($iLang) . $sUrl, true, $aParams, $iLang);
    }
}