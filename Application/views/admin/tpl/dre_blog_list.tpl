<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<title>[{ oxmultilang ident="MAIN_TITLE" }]</title>
	<link rel="stylesheet" href="[{$oViewConf->getResourceUrl()}]main.css">
	<link rel="stylesheet" href="[{$oViewConf->getResourceUrl()}]colors.css">
	<meta http-equiv="Content-Type" content="text/html; charset=[{$charset}]">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="../modules/bender/dre_blog/out/src/css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="../modules/bender/dre_blog/out/src/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="../modules/bender/dre_blog/out/src/js/jquery-3.1.1.min.js"></script>
	<script src="../modules/bender/dre_blog/out/src/js/bootstrap.min.js"></script>
	<script src="../modules/bender/dre_blog/out/src/js/blog.js"></script>

</head>
<body>
<div class="row">
	<div class="container-fluid">

		<button type="button" class="btn btn-primary" onclick="addNewEmptyBlog('neuerBlog')">
			+ neuer Blogeintrag
		</button>

		<div id="testbereich">leer</div>

        [{foreach from=$oView->getBlogList() item=blog name=blog}]
			<div class="jumbotron">
				<h3>
					[{$blog->oxblog__oxtitle->value}]
					<small>[{$blog->oxblog__oxtimestamp->value|date_format:"%d.%m.%Y"}]</small>
				</h3>
				<p>
					[{$blog->oxblog__oxshortdesc->value}]
				</p>
				<p>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#[{$blog->oxblog__oxid->value}]">
						Bearbeiten
					</button>
				</p>
			</div>
	        <div class="modal fade" id="[{$blog->oxblog__oxid->value}]" tabindex="-1" role="dialog" aria-labelledby="meinModalLabel">
		        <div class="modal-dialog">
			        <div class="modal-content">
				        <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Schließen"><span
								        aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title">[{$blog->oxblog__oxtitle->value}]</h4>
				        </div>
				        <div class="modal-body">
					        <h4>Kurzbeschreibung</h4>
					        <p>[{$blog->oxblog__oxshortdesc->value}]</p>
					        <h4>Langtext</h4>
					        <p>[{$blog->oxblog__oxlongdesc->value}]</p>
				        </div>
				        <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
					        <button type="button" class="btn btn-primary">Änderungen speichern</button>
				        </div>
			        </div><!-- /.modal-content -->
		        </div><!-- /.modal-dialog -->
	        </div>
	        <!-- /.modal -->
            [{*$blog|var_dump*}]
        [{/foreach}]
	</div>
</div>

</body>
</html>
[{*$oView|var_dump*}]