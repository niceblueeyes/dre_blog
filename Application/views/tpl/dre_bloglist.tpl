[{capture append="oxidBlock_content"}]
    [{assign var="bloglist" value=$oView->getBlogList()}]
	<h1>Blogliste</h1>

	erster Design Vorschlag:
    [{foreach from=$oView->getBlogList() item=blog name=blog}]
		<div class="jumbotron">
			<h3>[{$blog->oxblog__oxtitle->value}]
				<small>[{$blog->oxblog__oxtimestamp->value|date_format:"%d.%m.%Y"}]</small></h3>
			<p>
				[{$blog->oxblog__oxshortdesc->value}]
			</p>
			<p>
				<a class="btn btn-primary btn-lg" href="[{$oViewConf->getCurrentHomeDir()}]index.php?cl=dre_blog&blogid=[{$blog->oxblog__oxid->value}]" role="button">
					Mehr erfahren
				</a>
			</p>
		</div>
        [{*$blog|var_dump*}]
    [{/foreach}]

	zweiter Design Vorschlag:
	<div class="row">
	    [{foreach from=$oView->getBlogList() item=blog name=blog}]
			<div class="col-sm-3 col-md-2">
				<div class="thumbnail">
					<img src="[{$oViewConf->getCurrentHomeDir()}]out/pictures/blog/[{$blog->getPicture()}]" alt="...">
					<div class="caption">
						<h3>[{$blog->oxblog__oxtitle->value}]
							<small>[{$blog->oxblog__oxtimestamp->value|date_format:"%d.%m.%Y"}]</small></h3>
						<p>[{$blog->getLink()}]</p>
						<p>[{$blog->oxblog__oxshortdesc->value}]</p>
						<p><a href="[{$oViewConf->getCurrentHomeDir()}]index.php?cl=dre_blog&blogid=[{$blog->oxblog__oxid->value}]" class="btn btn-default" role="button">Mehr erfahren</a></p>
					</div>
				</div>
			</div>
		[{/foreach}]
	</div>
    [{*$oView->getBlogList()|var_dump*}]

    [{*}]

	[{assign var="template_title" value=$oView->getTitle()}]


	<div class="cmsContent">
		[{$oView->getContent()}]

	</div>
	[{*}]
    [{*if $oView->getActionProducts() }]
        [{include file="widget/product/list.tpl" type='grid' listId="dreLP" products=$oView->getActionProducts() showMainLink=true}]
    [{/if*}]
    [{insert name="oxid_tracker" title=$template_title }]
[{/capture}]
[{include file="layout/page.tpl"}]