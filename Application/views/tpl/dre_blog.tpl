[{capture append="oxidBlock_content"}]
    [{assign var="blog" value=$oView->getBlog()}]

    <div class="container">
        <h1>Detail Blog</h1>
        <img src="[{$oViewConf->getCurrentHomeDir()}]out/pictures/blog/[{$blog->oxblog__oxpic->value}]"/>
        <div class="post-content">
            <div class="post-info top">
                <span class="post-date">
                    [{$blog->oxblog__oxtimestamp->value|date_format:"%d.%m.%Y"}]
                </span>
            </div>
            <div class="post-title">
                <h1>[{$blog->oxblog__oxtitle->value}]</h1>
            </div>
            <div class="post-body">
                [{$blog->getLongDesc()}]
            </div>
        </div>
    </div>



    [{$blog|var_dump}]

    [{*foreach from=$oView->getBlogList() item=blog name=blog}]
        <div class="jumbotron">
            <h3>[{$blog->oxblog__oxtitle->value}] <small>[{$blog->oxblog__oxtimestamp->value|date_format:"%d.%m.%Y"}]</small></h3>
            <p>
                kurzbeschreibung
            </p>
            <p>
                <a class="btn btn-primary btn-lg" href="#" role="button">
                    Mehr erfahren
                </a>
            </p>
        </div>

        [{*$blog|var_dump}]


    [{/foreach*}]
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="..." alt="...">
                <div class="caption">
                    <h3>Vorschaubild-Titel</h3>
                    <p>kurzbeschreibung</p>
                    <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default"
                                                                                       role="button">Button</a></p>
                </div>
            </div>
        </div>
    </div>

    [{*$oView->getBlogList()|var_dump*}]

	[{*}]

    [{assign var="template_title" value=$oView->getTitle()}]


	<div class="cmsContent">
        [{$oView->getContent()}]

	</div>
	[{*}]
    [{*if $oView->getActionProducts() }]
        [{include file="widget/product/list.tpl" type='grid' listId="dreLP" products=$oView->getActionProducts() showMainLink=true}]
    [{/if*}]
    [{insert name="oxid_tracker" title=$template_title }]
[{/capture}]
[{include file="layout/page.tpl"}]