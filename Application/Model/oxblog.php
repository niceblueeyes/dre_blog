<?php

namespace Bender\dre_Blog\Application\Model;

use OxidEsales\Eshop\Application\Model\Article;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Model\BaseModel;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Request;
use OxidEsales\Eshop\Core\UtilsFile;
use OxidEsales\Eshop\Core\UtilsUrl;
use OxidEsales\Eshop\Core\UtilsView;

/**
 * Article actions manager. Collects and keeps blog of chosen article.
 *
 */
class oxblog extends oxblog_parent
{
    /**
     * Current class name
     *
     * @var string
     */
    protected $_sClassName = 'oxblog';

    /**
     * Class constructor. Executes oxblog::init(), initiates parent constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->init('oxblog');
    }


    public function getPicture()
    {
        if ($this != null) {
            return $this->oxblog__oxpic->value;
        }
    }

    /**
     * Adds an article to this actions
     *
     * @param string $articleId id of the article to be added
     */
    public function addArticle($articleId)
    {
        $oDb = DatabaseProvider::getDb();
        $sQ = "select max(oxsort) from oxblog2article where oxactionid = " . $oDb->quote($this->getId()) . " and oxshopid = '" . $this->getShopId() . "'";
        $iSort = ((int)$oDb->getOne($sQ)) + 1;

        $oNewGroup = oxNew(BaseModel::class);
        $oNewGroup->init('oxblog2article');
        $oNewGroup->oxblog2article__oxshopid = new Field($this->getShopId());
        $oNewGroup->oxblog2article__oxactionid = new Field($this->getId());
        $oNewGroup->oxblog2article__oxartid = new Field($articleId);
        $oNewGroup->oxblog2article__oxsort = new Field($iSort);
        $oNewGroup->save();
    }

    /**
     * Removes an article from this actions
     *
     * @param string $articleId id of the article to be removed
     *
     * @return bool
     */
    public function removeArticle($articleId)
    {
        // remove actions from articles also
        $oDb = DatabaseProvider::getDb();
        $sDelete = "delete from oxblog2article where oxactionid = " . $oDb->quote($this->getId()) . " and oxartid = " . $oDb->quote($articleId) . " and oxshopid = '" . $this->getShopId() . "'";
        $iRemovedArticles = $oDb->execute($sDelete);

        return (bool)$iRemovedArticles;
    }

    /**
     * Removes article action, returns true on success. For
     * performance - you can not load action object - just pass
     * action ID.
     *
     * @param string $articleId Object ID
     *
     * @return bool
     */
    public function delete($articleId = null)
    {
        $articleId = $articleId ? $articleId : $this->getId();
        if (!$articleId) {
            return false;
        }

        // remove actions from articles also
        $oDb = DatabaseProvider::getDb();
        $sDelete = "delete from oxblog2article where oxactionid = " . $oDb->quote($articleId) . " and oxshopid = '" . $this->getShopId() . "'";
        $oDb->execute($sDelete);

        return parent::delete($articleId);
    }

    /**
     * return time left until finished
     *
     * @return int
     */
    public function getTimeLeft()
    {
        $iNow = Registry::getUtilsDate()->getTime();
        $iFrom = strtotime($this->oxblog__oxactiveto->value);

        return $iFrom - $iNow;
    }

    /**
     * return time left until start
     *
     * @return int
     */
    public function getTimeUntilStart()
    {
        $iNow = Registry::getUtilsDate()->getTime();
        $iFrom = strtotime($this->oxblog__oxactivefrom->value);

        return $iFrom - $iNow;
    }

    /**
     * start the promotion NOW!
     */
    public function start()
    {
        $this->oxblog__oxactivefrom = new Field(date('Y-m-d H:i:s', Registry::getUtilsDate()->getTime()));
        if ($this->oxblog__oxactiveto->value && ($this->oxblog__oxactiveto->value != '0000-00-00 00:00:00')) {
            $iNow = Registry::getUtilsDate()->getTime();
            $iTo = strtotime($this->oxblog__oxactiveto->value);
            if ($iNow > $iTo) {
                $this->oxblog__oxactiveto = new Field('0000-00-00 00:00:00');
            }
        }
        $this->save();
    }

    /**
     * stop the promotion NOW!
     */
    public function stop()
    {
        $this->oxblog__oxactiveto = new Field(date('Y-m-d H:i:s', Registry::getUtilsDate()->getTime()));
        $this->save();
    }

    /**
     * check if this action is active
     *
     * @return bool
     */
    public function isRunning()
    {
        if (!($this->oxblog__oxactive->value
            && $this->oxblog__oxtype->value == 2
            && $this->oxblog__oxactivefrom->value != '0000-00-00 00:00:00'
        )
        ) {
            return false;
        }
        $iNow = Registry::getUtilsDate()->getTime();
        $iFrom = strtotime($this->oxblog__oxactivefrom->value);
        if ($iNow < $iFrom) {
            return false;
        }

        if ($this->oxblog__oxactiveto->value != '0000-00-00 00:00:00') {
            $iTo = strtotime($this->oxblog__oxactiveto->value);
            if ($iNow > $iTo) {
                return false;
            }
        }

        return true;
    }

    /**
     * get long description, parsed through smarty
     *
     * @return string
     */
    public function getLongDesc()
    {
        /** @var UtilsView $oUtilsView */
        $oUtilsView = Registry::getUtilsView();
        return $oUtilsView->parseThroughSmarty($this->oxblog__oxlongdesc->getRawValue(), $this->getId() . $this->getLanguage(), null, true);
    }

    /**
     * return assigned banner article
     *
     * @return oxArticle
     */
    public function getBannerArticle()
    {
        $sArtId = $this->fetchBannerArticleId();

        if ($sArtId) {
            $oArticle = oxNew(Article::class);

            if ($this->isAdmin()) {
                $oArticle->setLanguage(Registry::getLang()->getEditLanguage());
            }

            if ($oArticle->load($sArtId)) {
                return $oArticle;
            }
        }

        return null;
    }


    /**
     * Fetch the oxobjectid of the article corresponding this action.
     *
     * @return string The id of the oxobjectid belonging to this action.
     */
    protected function fetchBannerArticleId()
    {
        $database = DatabaseProvider::getDb();

        $articleId = $database->getOne(
            'select oxobjectid from oxobject2blog ' .
            'where oxactionid=' . $database->quote($this->getId()) .
            ' and oxclass="oxarticle"'
        );

        return $articleId;
    }

    /**
     * Returns assigned banner article picture url
     *
     * @return string
     */
    public function getBannerPictureUrl()
    {
        if (isset($this->oxblog__oxpic) && $this->oxblog__oxpic->value) {
            $sPromoDir = Registry::getUtilsFile()->normalizeDir(UtilsFile::PROMO_PICTURE_DIR);

            return $this->getConfig()->getPictureUrl($sPromoDir . $this->oxblog__oxpic->value, false);
        }
    }

    /**
     * Returns assigned banner link. If no link is defined and article is
     * assigned to banner, article link will be returned.
     *
     * @return string
     */
    public function getBannerLink()
    {
        $sUrl = null;

        if (isset($this->oxblog__oxlink) && $this->oxblog__oxlink->value) {
            /** @var UtilsUrl $oUtilsUlr */
            $oUtilsUlr = Registry::getUtilsUrl();
            $sUrl = $oUtilsUlr->addShopHost($this->oxblog__oxlink->value);
            $sUrl = $oUtilsUlr->processUrl($sUrl);
        } else {
            if ($oArticle = $this->getBannerArticle()) {
                // if article is assigned to banner, getting article link
                $sUrl = $oArticle->getLink();
            }
        }

        return $sUrl;
    }

    /**
     * Returns true if Action is default.
     *
     * @return bool
     */
    public function isDefault()
    {
        return '0' === $this->oxblog__oxtype->value;
    }

    public function getLink($iLang = null)
    {
        if (!isset($iLang)) {
            $iLang = Registry::getLang()->getBaseLanguage();
        }

        $oConf = $this->getConfig();

        $request = oxNew(Request::class);
        $sblogid = $request->getRequestParameter('blogid');

        $requestparams = null;
        $url = 'cl=dre_blog&amp';


        $sUrl = 'index.php?cl=dre_blog&amp;blogid=' . $this->oxblog__oxid->value;

        $oSeoEncoder = oxNew('oxseoencoder');
        $sSeoUrl = $oSeoEncoder->fetchSeoUrl($sUrl, $iLang);

        if ($sSeoUrl) {
            return $oConf->getCurrentShopUrl() . $sSeoUrl;
        }

        $aParams['blogid'] = $sblogid;
        return Registry::get("oxUtilsUrl")->processUrl($oConf->getShopCurrentURL($iLang) . $sUrl, true, $aParams, $iLang);
    }



    /**
     * Returns name of current action function
     *
     * @return string
     */
    public function getFncName()
    {
        return $this->_sFnc;
    }
}
