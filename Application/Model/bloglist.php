<?php

namespace Bender\dre_Blog\Application\Model;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;

/**
 * Promotion List manager.
 *
 */
class bloglist extends bloglist_parent
{
    /**
     * List Object class name
     *
     * @var string
     */
    protected $_sObjectsInListName = \Bender\dre_Blog\Application\Model\oxblog::class;

    protected $_sClassName = 'bloglist';

    public function loadBlogListe(){
        $sViewName = $this->getBaseObject()->getViewName();

        $oDb = DatabaseProvider::getDb();
        $sQ = "select * from {$sViewName} where oxshopid='". $this->getConfig()->getShopId() ."' and oxactive = 1";
        $this->selectString($sQ);

    }



    /**
     * Loads x last finished promotions
     *
     * @param int $iCount count to load
     */
    public function loadFinishedByCount($iCount)
    {
        $sViewName = $this->getBaseObject()->getViewName();
        $sDate = date('Y-m-d H:i:s', Registry::getUtilsDate()->getTime());

        $oDb = DatabaseProvider::getDb();
        $sQ = "select * from {$sViewName} where oxtype=2 and oxactive=1 and oxshopid='" . $this->getConfig()->getShopId() . "' and oxactiveto>0 and oxactiveto < " . $oDb->quote($sDate) . "
               " . $this->_getUserGroupFilter() . "
               order by oxactiveto desc, oxactivefrom desc limit " . (int)$iCount;
        $this->selectString($sQ);
        $this->_aArray = array_reverse($this->_aArray, true);
    }

    /**
     * Loads last finished promotions after given timespan
     *
     * @param int $iTimespan timespan to load
     */
    public function loadFinishedByTimespan($iTimespan)
    {
        $sViewName = $this->getBaseObject()->getViewName();
        $sDateTo = date('Y-m-d H:i:s', Registry::getUtilsDate()->getTime());
        $sDateFrom = date('Y-m-d H:i:s', Registry::getUtilsDate()->getTime() - $iTimespan);
        $oDb = DatabaseProvider::getDb();
        $sQ = "select * from {$sViewName} where oxtype=2 and oxactive=1 and oxshopid='" . $this->getConfig()->getShopId() . "' and oxactiveto < " . $oDb->quote($sDateTo) . " and oxactiveto > " . $oDb->quote($sDateFrom) . "
               " . $this->_getUserGroupFilter() . "
               order by oxactiveto, oxactivefrom";
        $this->selectString($sQ);
    }

    /**
     * Loads current promotions
     */
    public function loadCurrent()
    {
        $sViewName = $this->getBaseObject()->getViewName();
        $sDate = date('Y-m-d H:i:s', Registry::getUtilsDate()->getTime());
        $oDb = DatabaseProvider::getDb();
        $sQ = "select * from {$sViewName} where oxtype=2 and oxactive=1 and oxshopid='" . $this->getConfig()->getShopId() . "' and (oxactiveto > " . $oDb->quote($sDate) . " or oxactiveto=0) and oxactivefrom != 0 and oxactivefrom < " . $oDb->quote($sDate) . "
               " . $this->_getUserGroupFilter() . "
               order by oxactiveto, oxactivefrom";
        $this->selectString($sQ);
    }

    /**
     * Loads next not yet started promotions by cound
     *
     * @param int $iCount count to load
     */
    public function loadFutureByCount($iCount)
    {
        $sViewName = $this->getBaseObject()->getViewName();
        $sDate = date('Y-m-d H:i:s', Registry::getUtilsDate()->getTime());
        $oDb = DatabaseProvider::getDb();
        $sQ = "select * from {$sViewName} where oxtype=2 and oxactive=1 and oxshopid='" . $this->getConfig()->getShopId() . "' and (oxactiveto > " . $oDb->quote($sDate) . " or oxactiveto=0) and oxactivefrom > " . $oDb->quote($sDate) . "
               " . $this->_getUserGroupFilter() . "
               order by oxactiveto, oxactivefrom limit " . (int)$iCount;
        $this->selectString($sQ);
    }

    /**
     * Loads next not yet started promotions before the given timespan
     *
     * @param int $iTimespan timespan to load
     */
    public function loadFutureByTimespan($iTimespan)
    {
        $sViewName = $this->getBaseObject()->getViewName();
        $sDate = date('Y-m-d H:i:s', Registry::getUtilsDate()->getTime());
        $sDateTo = date('Y-m-d H:i:s', Registry::getUtilsDate()->getTime() + $iTimespan);
        $oDb = DatabaseProvider::getDb();
        $sQ = "select * from {$sViewName} where oxtype=2 and oxactive=1 and oxshopid='" . $this->getConfig()->getShopId() . "' and (oxactiveto > " . $oDb->quote($sDate) . " or oxactiveto=0) and oxactivefrom > " . $oDb->quote($sDate) . " and oxactivefrom < " . $oDb->quote($sDateTo) . "
               " . $this->_getUserGroupFilter() . "
               order by oxactiveto, oxactivefrom";
        $this->selectString($sQ);
    }

    /**
     * Returns part of user group filter query
     *
     * @param \OxidEsales\Eshop\Application\Model\User $oUser user object
     *
     * @return string
     */
    protected function _getUserGroupFilter($oUser = null)
    {
        $oUser = ($oUser == null) ? $this->getUser() : $oUser;
        $sTable = getViewName('oxblog');
        $sGroupTable = getViewName('oxgroups');

        $aIds = [];
        // checking for current session user which gives additional restrictions for user itself, users group and country
        if ($oUser && count($aGroupIds = $oUser->getUserGroups())) {
            foreach ($aGroupIds as $oGroup) {
                $aIds[] = $oGroup->getId();
            }
        }

        $sGroupSql = count($aIds) ? "EXISTS(select oxobject2blog.oxid from oxobject2blog where oxobject2blog.oxblogid=$sTable.OXID and oxobject2blog.oxclass='oxgroups' and oxobject2blog.OXOBJECTID in (" . implode(', ', DatabaseProvider::getDb()->quoteArray($aIds)) . ") )" : '0';
        return " and (
            select
                if(EXISTS(select 1 from oxobject2blog, $sGroupTable where $sGroupTable.oxid=oxobject2blog.oxobjectid and oxobject2blog.oxblogid=$sTable.OXID and oxobject2blog.oxclass='oxgroups' LIMIT 1),
                    $sGroupSql,
                    1)
            ) ";
    }

    /**
     * return true if there are any active promotions
     *
     * @return boolean
     */
    public function areAnyActivePromotions()
    {
        return (bool)$this->fetchExistsActivePromotion();
    }


    /**
     * Fetch the information, if there is an active promotion.
     *
     * @return string One, if there is an active promotion.
     */
    protected function fetchExistsActivePromotion()
    {
        $query = "select 1 from " . getViewName('oxblog') . " where oxtype=2 and oxactive=1 and oxshopid='" . $this->getConfig()->getShopId() . "' limit 1";

        return DatabaseProvider::getDb()->getOne($query);
    }

    /**
     * load active shop banner list
     */
    public function loadBanners()
    {
        $oBaseObject = $this->getBaseObject();
        $oViewName = $oBaseObject->getViewName();
        $sQ = "select * from {$oViewName} where oxtype=3 and " . $oBaseObject->getSqlActiveSnippet()
            . " and oxshopid='" . $this->getConfig()->getShopId() . "' " . $this->_getUserGroupFilter()
            . " order by oxsort";
        $this->selectString($sQ);
    }
}

