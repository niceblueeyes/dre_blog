<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

$aModule = [
    'id'          => 'dre_blog',
    'title'       => [
        'en' => '<img src="../modules/bender/dre_blog/out/img/favicon.ico" title="Bodynova Blog Modul">odynova Blog Modul',
        'de' => '<img src="../modules/bender/dre_blog/out/img/favicon.ico" title="Bodynova Blog Modul">odynova Blog',
    ],
    'description' => [
        'en' => 'Blog scripts',
        'de' => 'Blog Modul erstellt einfache Landing Pages',
    ],
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'     => '2.0',
    'author'      => 'Bodynova GmbH',
    'url'         => 'https://bodynova.de',
    'email'       => 'support@bodynova.de',
    'controllers'       => [
        'dre_blog' => \Bender\dre_Blog\Application\Controller\dre_blog::class,
        'dre_bloglist' => \Bender\dre_Blog\Application\Controller\dre_bloglist::class,
        'dre_blog_listcontroller' => \Bender\dre_Blog\Application\Controller\Admin\dre_blog_listcontroller::class,
    ],
    'extend'      => [
        \OxidEsales\Eshop\Core\Model\MultiLanguageModel::class =>
            \Bender\dre_Blog\Application\Model\oxblog::class,
       \OxidEsales\Eshop\Core\Model\ListModel::class =>
           \Bender\dre_Blog\Application\Model\bloglist::class,
    ],
    'templates'   => [
        'dre_blog.tpl' => 'bender/dre_blog/Application/views/tpl/dre_blog.tpl',
        'dre_bloglist.tpl' => 'bender/dre_blog/Application/views/tpl/dre_bloglist.tpl',
        'dre_blog_list.tpl' => 'bender/dre_blog/Application/views/admin/tpl/dre_blog_list.tpl'
    ],
    'blocks'      => [
        array(
            'template' => 'layout/base.tpl',
            'block' => 'base_style',
            'file' => 'Application/views/blocks/dre_base_style.tpl'
        ),
    ],
    'settings'    => [],
];