<?php

use OxidEsales\Eshop\Core\DatabaseProvider;

include '../../../../bootstrap.php';

header("Content-type: application/json; charset=utf-8");

$data = json_decode(file_get_contents('php://input'), true);

addNewEmptyBlog($data);

//$odb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);


function addNewEmptyBlog($data){
    echo json_encode($data);
}